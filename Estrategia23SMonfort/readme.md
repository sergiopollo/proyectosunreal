ESTRATÈGIA - Sergio Monfort

Risketos Bàsics

 - [X] Capacitat d’elecció, per exemple diferents torretes.
 - [X] El HUD es crea i s’emplena per BP.
 - [X] Ús d’esdeveniments i dispatchers, tot ha d’estar desacoplat.
 - [X] Enemics o objectius a on atacar i en fer-ho que morin.
 - [X] Ús d’un actor component o Interfície.

Risketos Opcionals

 - [X] Augment progressiu de dificultat.
 - [X] Sistema de compra per diners per construir les torretes.
 - [X] Preview de la construcció de les torretes (amb rang)

Controls

 - RIGHT CLICK per mostrar el menu de construcció / tancar el menú / cancelar la construcció de la torreta seleccioanda
 - LEFT CLICK per seleccionar la torreta / construir la torreta seleccionada