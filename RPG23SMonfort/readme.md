RPG - Sergio Monfort

Risketos Bàsics

 - [X] Todos


Risketos Opcionals

 - [X] Sistema de combat per torns

Controls
   
   Sempre:
 - SPACEBAR per entrar i sortir de combat

   Fora de Combat:
 - LEFT CLICK per moure't a la posició on has fet click
 - LEFT CLICK HOLD per moure't en direcció al cursor

   Dins de Combat:
 - LEFT CLICK per disparar l'habilitat seleccionada en direcció al cursor (s'ha de fer click sobre un enemic o no funciona)
 - 1 per seleccionar la habilitat 1
 - 2 per seleccionar la habilitat 2
 - 3 per seleccionar la habilitat 3