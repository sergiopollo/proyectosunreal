// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23SMonfortGameMode.h"
#include "RPG23SMonfortPlayerController.h"
#include "RPG23SMonfortCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARPG23SMonfortGameMode::ARPG23SMonfortGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARPG23SMonfortPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}