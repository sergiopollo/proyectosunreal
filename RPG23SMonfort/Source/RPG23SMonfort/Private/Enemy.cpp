// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EnemyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = EnemyRoot;
	
	EnemyBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EnemyBody"));
	EnemyBody->SetupAttachment(EnemyRoot);
}

FSkillDataRow* AEnemy::GetSkill(ESkill aSkill)
{
	FSkillDataRow* SkillFound {};

	if(mSkillDB)
	{
		FName SkillString {UEnum::GetDisplayValueAsText(aSkill).ToString()};
		static const FString FindContext {FString("Searching for ").Append(SkillString.ToString())};
		SkillFound=mSkillDB->FindRow<FSkillDataRow>(SkillString, FindContext, true);
	}

	return SkillFound;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

