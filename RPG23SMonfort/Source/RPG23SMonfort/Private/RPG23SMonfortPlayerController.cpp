// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23SMonfortPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "RPG23SMonfortCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "RPG23SMonfort/Public/Utils.h"

ARPG23SMonfortPlayerController::ARPG23SMonfortPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void ARPG23SMonfortPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(
		GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void ARPG23SMonfortPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(mInputConfigData->InputMouseClick, ETriggerEvent::Started, this,
		                                   &ARPG23SMonfortPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(mInputConfigData->InputMouseClick, ETriggerEvent::Triggered, this,
		                                   &ARPG23SMonfortPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(mInputConfigData->InputMouseClick, ETriggerEvent::Completed, this,
		                                   &ARPG23SMonfortPlayerController::OnSetDestinationReleased);

		EnhancedInputComponent->BindAction(mInputConfigData->InputAct, ETriggerEvent::Started, this, &ARPG23SMonfortPlayerController::OnActPressed);

		EnhancedInputComponent->BindAction(mInputConfigData->InputSpaceBar, ETriggerEvent::Started, this, &ARPG23SMonfortPlayerController::OnSpacebarPressed);
		
		for (int ButtonIndex {0}; ButtonIndex < mInputConfigData->InputSkills.Num(); ++ButtonIndex)
		{
			EnhancedInputComponent->BindAction(mInputConfigData->InputSkills[ButtonIndex], ETriggerEvent::Completed, this, &ARPG23SMonfortPlayerController::OnSkillPressed, ButtonIndex);
		}
	}

}

void ARPG23SMonfortPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void ARPG23SMonfortPlayerController::OnSetDestinationTriggered()
{
	if(this->CombatState == ECombatState::OVERWORLD)
	{
		// We flag that the input is being pressed
		FollowTime += GetWorld()->GetDeltaSeconds();

		// We look for the location in the world where the player has pressed the input
		FHitResult Hit;
		bool bHitSuccessful = false;
		if (bIsTouch)
		{
			bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
		}
		else
		{
			bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
		}

		// If we hit a surface, cache the location
		if (bHitSuccessful)
		{
			CachedDestination = Hit.Location;
		}

		// Move towards mouse pointer or touch
		APawn* ControlledPawn = GetPawn();
		if (ControlledPawn != nullptr)
		{
			FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
			ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
		}
	}
}

void ARPG23SMonfortPlayerController::OnSetDestinationReleased()
{
	if(this->CombatState == ECombatState::OVERWORLD)
	{
		// If it was a short press
		if (FollowTime <= ShortPressThreshold)
		{
			// We move there and spawn some particles
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator,
														   FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
		}

		FollowTime = 0.f;
	}
}

void ARPG23SMonfortPlayerController::OnSkillPressed(int aButtonPressed)
{
	if(this->CombatState == ECombatState::COMBATTURN)
	{
		//auto -> es como poner 'var' sirve para no tener que poner el tipo, pero no se puede cambiar
		//meter una declaracion de funcion dentro de un if -> para que esa variable se borre despues de salir del if pero haga la comprobacion antes de entrar
		if(auto SkillPressed {mSkills[aButtonPressed]}; SkillPressed != ESkill::NONE)
		{
			//llama a la funcion getskill y lo que reciba lo guarda en la variable Skill, luego se comprueba si no es nula (despues del ; )
			if(auto* Skill {GetSkill(SkillPressed)}; Skill)
			{
				FString SkillString {UEnum::GetDisplayValueAsText(Skill->Name).ToString()};
				ScreenPrint(Format1("Skill %s", *SkillString));
				ScreenPrint(Format1("Skill %d", Skill->Name));
				//*Skill es el valor que hay en el puntero (Skill es un puntero, esta declarado encima)
				mSelectedSkill =  *Skill;
			}
		}
	}
}

void ARPG23SMonfortPlayerController::OnActPressed()
{
	if(this->CombatState == ECombatState::COMBATTURN)
	{
		if(mSelectedSkill.Name == ESkill::NONE) return;

		FVector HitLocation {FVector::ZeroVector};
		GetHitResultUnderCursorByChannel(TraceTypeQuery1, true, mHitResult);

		evOnLocationClick.Broadcast(mHitResult.Location, mSelectedSkill);

		//this->CombatState = COMBATNOTTURN;
		//ScreenPrintText("ahora no es mi turno")
	}
}

void ARPG23SMonfortPlayerController::OnSpacebarPressed()
{
	ScreenPrintText("pressed spacebar ohmaygah")
	evOnSpacebarPressed.Broadcast();
	
}

FSkillDataRow* ARPG23SMonfortPlayerController::GetSkill(ESkill aSkill)
{
	FSkillDataRow* SkillFound {};

	if(mSkillDB)
	{
		FName SkillString {UEnum::GetDisplayValueAsText(aSkill).ToString()};
		static const FString FindContext {FString("Searching for ").Append(SkillString.ToString())};
		SkillFound=mSkillDB->FindRow<FSkillDataRow>(SkillString, FindContext, true);
	}

	return SkillFound;
}
