// Fill out your copyright notice in the Description page of Project Settings.


#include "SkillBase.h"

#include "Enemy.h"
#include "RPG23SMonfortCharacter.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "RPG23SMonfort/Public/Utils.h"

// Sets default values
ASkillBase::ASkillBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mpSphereColl = CreateDefaultSubobject<USphereComponent>(TEXT("SphereColl"));

	mpSphereColl->OnComponentBeginOverlap.AddDynamic(this, &ASkillBase::OnCollisionEnterCustom);

}

// Called when the game starts or when spawned
void ASkillBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASkillBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	/*
	const UWorld* pWorld {GetWorld()};
	if(pWorld==nullptr) return;
	
	const FVector StartLocation {this->GetActorLocation()};

	const FVector EndLocation {StartLocation+(UKismetMathLibrary::GetForwardVector(this->K2_GetActorRotation()) * 1)};

	FCollisionQueryParams QueryParams {};

	FHitResult HitResult {};
	pWorld->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera, QueryParams);
	//DrawDebugLine(pWorld, StartLocation, EndLocation, HitResult.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);

	if(HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
	{
		ScreenPrintWithFormat(Format1("%s",*HitResult.GetActor()->GetName()));
		ACharacter* HitChar {Cast<ACharacter>(HitResult.GetActor())};
		
		if(isPlayerAbility)
		{
			if(HitResult.GetActor()->GetClass()->IsChildOf(AEnemy::StaticClass()))
			{
				UGameplayStatics::ApplyDamage(HitResult.GetActor(), 1, UGameplayStatics::GetPlayerController(pWorld, 0), this->GetOwner(), {});
				AActor::Destroy();
			}
		}
		else
		{
			if(HitResult.GetActor()->GetClass()->IsChildOf(ARPG23SMonfortCharacter::StaticClass()))
			{
				UGameplayStatics::ApplyDamage(HitChar->GetController(), 1, UGameplayStatics::GetPlayerController(pWorld, 0), this->GetOwner(), {});
				AActor::Destroy();
			}
		}
		
	}
	*/
}


void ASkillBase::OnCollisionEnterCustom(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//ScreenPrintText("LANZO HECHIZO");
	//ScreenPrintWithFormat(Format1("%s", *OtherActor->GetName()));

	const UWorld* pWorld {GetWorld()};
	if(pWorld==nullptr) return;
	
	ACharacter* HitChar {Cast<ACharacter>(OtherActor)};
		
	if(isPlayerAbility)
	{
		if(OtherActor->GetClass()->IsChildOf(AEnemy::StaticClass()))
		{
			UGameplayStatics::ApplyDamage(OtherActor, 1, UGameplayStatics::GetPlayerController(pWorld, 0), this->GetOwner(), {});
			AActor::Destroy();
		}
	}
	else
	{
		if(OtherActor->GetClass()->IsChildOf(ARPG23SMonfortCharacter::StaticClass()))
		{
			UGameplayStatics::ApplyDamage(HitChar->GetController(), 1, UGameplayStatics::GetPlayerController(pWorld, 0), this->GetOwner(), {});
			AActor::Destroy();
		}
	}
	
}

