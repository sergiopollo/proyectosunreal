// Copyright Epic Games, Inc. All Rights Reserved.

#include "RPG23SMonfort.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RPG23SMonfort, "RPG23SMonfort" );

DEFINE_LOG_CATEGORY(LogRPG23SMonfort)
 