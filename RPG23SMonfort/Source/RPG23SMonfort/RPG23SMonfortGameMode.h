// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPG23SMonfortGameMode.generated.h"

/* DELEGADOS
 * EnemyDeath
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEnemyDeath, class AEnemy*, enemy);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTurnChange);

UCLASS(minimalapi)
class ARPG23SMonfortGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARPG23SMonfortGameMode();

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnEnemyDeath evOnEnemyDeath;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnTurnChange evOnTurnChange;
};



