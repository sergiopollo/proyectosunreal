#pragma once

#define ScreenPrint(x) if(GEngine) {GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, x);}

#define LogPrint(x) UE_LOG(LogTemp, Warning, TEXT(x));

#define ScreenPrintText(x) if(GEngine) {GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, FString::Printf(TEXT(x)));}

#define ScreenPrintWithFormat(x) if(GEngine) {GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow,x);}

#define Format1(x, y) FString::Printf(TEXT(x), y)
