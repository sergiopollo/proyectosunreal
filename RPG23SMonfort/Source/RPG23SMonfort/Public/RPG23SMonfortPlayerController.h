// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FSkillDataRow.h"
#include "Templates/SubclassOf.h"
#include "GameFramework/PlayerController.h"
#include "InputActionValue.h"
#include "RPG23SMonfortPlayerController.generated.h"

/** Forward declaration to improve compiling times */
class UNiagaraSystem;
class UInputConfigData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnLocationClick, FVector, aClickLocation, const FSkillDataRow, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDirectionSkillCast, FRotator, aRotation, const FSkillDataRow, aSkillData);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSpacebarPressed);

UENUM(BlueprintType)
enum ECombatState
{
	OVERWORLD = 0 UMETA(DisplayName = "Overworld"),
	COMBATTURN UMETA(DisplayName = "combat turn"),
	COMBATNOTTURN UMETA(DisplayName = "combat not turn")
};

UCLASS()
class ARPG23SMonfortPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARPG23SMonfortPlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TEnumAsByte<ECombatState> CombatState {ECombatState::OVERWORLD};
	
	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* mInputConfigData;	

	
#pragma region SKILLS
	FSkillDataRow* GetSkill(ESkill skill);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = mSKILL)
		TArray<ESkill> mSkills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = mSKILL)
		UDataTable* mSkillDB;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = mSKILL)
		FSkillDataRow mSelectedSkill;
	
#pragma endregion	
	
#pragma region EVENTS

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnLocationClick evOnLocationClick;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnDirectionSkillCast evOnDirectionSkillCast;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FSpacebarPressed evOnSpacebarPressed;
	
#pragma endregion

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	virtual void SetupInputComponent() override;
	
	// To add mapping context
	virtual void BeginPlay();

	/** Input handlers for SetDestination action. */
	void OnInputStarted();
	void OnSetDestinationTriggered();
	void OnSetDestinationReleased();
	void OnTouchTriggered();
	void OnTouchReleased();

	void OnSkillPressed(int aButtonPressed);
	void OnActPressed();
	void OnSpacebarPressed();
	
private:
	FHitResult mHitResult {};
	FVector CachedDestination;

	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


