// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FSkillDataRow.h"
#include "GameFramework/Actor.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEnemyAttack);

UCLASS()
class RPG23SMONFORT_API AEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemy();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USceneComponent* EnemyRoot;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* EnemyBody;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FOnEnemyAttack evOnEnemyAttack;

#pragma region SKILLS
	
	FSkillDataRow* GetSkill(ESkill skill);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = mSKILL)
	TArray<ESkill> mSkills;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = mSKILL)
	UDataTable* mSkillDB;

#pragma endregion
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
