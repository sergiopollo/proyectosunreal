#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "FRPGClassDataRow.generated.h"

class UTexture2D;


UENUM(BlueprintType)
enum class ERPGClass : uint8
{
	NONE UMETA(DisplayName = "None"),
	WARRIOR UMETA(DisplayName = "Warrior"),
	MAGE UMETA(DisplayName = "Mage"),
	ENEMY UMETA(DisplayName = "Enemy")
};

USTRUCT(Blueprintable, BlueprintType)
struct FRPGClassDataRow : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ERPGClass Name {ERPGClass::NONE};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TWeakObjectPtr<UTexture2D> Image {nullptr};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int BaseHP;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int BaseAtk;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int BaseDef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int BaseSpeed;
	
};
