// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SkillBase.generated.h"

class USphereComponent;

UCLASS()
class RPG23SMONFORT_API ASkillBase : public AActor
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	void OnCollisionEnterCustom(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	                            const FHitResult& SweepResult);
	// Sets default values for this actor's properties
	ASkillBase();

	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		USphereComponent* mpSphereColl;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		AActor* mpTarget;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		float aDamage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		float mDuration;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta=(ExposeOnSpawn=true))
		bool isPlayerAbility;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
