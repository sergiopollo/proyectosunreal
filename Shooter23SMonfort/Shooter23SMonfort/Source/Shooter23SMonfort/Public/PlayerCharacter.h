// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "PlayerCharacter.generated.h"

class UBPC_Weapon;
class UInputMappingContext;
class UInputConfigData;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClick);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAddMoney, int, newMoney);

UCLASS()

class SHOOTER23SMONFORT_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UStaticMeshComponent* GetWeapon() const {return mWeaponPoint;}
	UCameraComponent* GetCamera() const {return mpCamera;}

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* mWeaponPoint;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputMappingContext* mInputMappingContext;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* mInputConfigData;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int mMoveSpeed {2U};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mCurrentHealth {100.f};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mMaxHealth {100.f};

	UPROPERTY(BlueprintAssignable)
		FOnClick evOnClick;

	UPROPERTY(BlueprintAssignable)
		FAddMoney evAddMoney;

	UBPC_Weapon* mpWeaponEquipped;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int mMoney {0};

	UFUNCTION(BlueprintCallable)
		void AddMoney(int newMoney);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	void MoveCallback(const FInputActionValue& aValue);

	void LookCallback(const FInputActionValue& aValue);

	void OnClickCallback(const FInputActionValue& aValue);
	
	UPROPERTY(EditAnywhere)
		UCameraComponent* mpCamera;

};
