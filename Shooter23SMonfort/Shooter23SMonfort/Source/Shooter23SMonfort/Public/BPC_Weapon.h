// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BPC_Weapon.generated.h"

class APlayerCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER23SMONFORT_API UBPC_Weapon : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBPC_Weapon();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void PickupWeapon(APlayerCharacter* apPlayerCharacter);

	UFUNCTION(BlueprintCallable)
		void OnFireCallback();

	UFUNCTION(BlueprintCallable)
		void Fire();

	UPROPERTY(EditDefaultsOnly)
		UBillboardComponent* mpMuzzleOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mShootRange {5000.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mDamage {2.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mBulletsPerShot {1.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mDispersion {0.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		APlayerCharacter* mpOwnerCharacter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int mPrice {1};
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	
		
};
