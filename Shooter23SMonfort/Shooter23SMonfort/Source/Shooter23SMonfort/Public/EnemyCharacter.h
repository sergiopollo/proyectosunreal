// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Perception/AISenseConfig_Sight.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class SHOOTER23SMONFORT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	// Sets default values for this character's properties
	AEnemyCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditDefaultsOnly)
		class UBoxComponent* mpBoxCollision;

	UFUNCTION()
		void OnHitCallback(AActor* apDamagedActor, float aDamage, const UDamageType* apDamageType, AController* apInstigatedBy, AActor* apDamageCauser);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		int mMoney {1};

	UPROPERTY(EditDefaultsOnly)
		float mMoveSpeed {600.0f};

	UPROPERTY(EditDefaultsOnly)
		float mHealth {10.0f};

	UPROPERTY(EditDefaultsOnly)
		float mDamage {1.0f};

#pragma region IA

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UAIPerceptionComponent* mAIPerception;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UAISenseConfig_Sight* mAISight;
	UFUNCTION()
		void OnSensedCallback(const TArray<AActor*>& Actors);
#pragma endregion
	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	void SetNewRotation(const FVector& aTargetRot);

	UPROPERTY(VisibleAnywhere)
		FRotator mCurrentRotation;

	UPROPERTY(VisibleAnywhere)
		FVector mSpawnPosition;
	
	UPROPERTY(VisibleAnywhere)
		FVector mCurrentVelocity;
	
	bool mGoBackToBase;
	FVector mNewPosition;
	float mDistanceSQRT;
	
};
