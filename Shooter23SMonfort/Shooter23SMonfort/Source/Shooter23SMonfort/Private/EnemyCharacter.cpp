// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"

#include "PlayerCharacter.h"
#include "Components/BoxComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Shooter23SMonfort/Public/Utils.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mpBoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	mpBoxCollision->SetupAttachment(RootComponent);
	
	OnTakeAnyDamage.AddDynamic(this, &AEnemyCharacter::OnHitCallback);

	//cosas IA
	mAISight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AISightConfig"));
	mAISight->SightRadius = 1250.f;
	mAISight->LoseSightRadius = 1300.f;
	mAISight->PeripheralVisionAngleDegrees=90.f;
	mAISight->DetectionByAffiliation.bDetectEnemies=true;
	mAISight->DetectionByAffiliation.bDetectFriendlies=true;
	mAISight->DetectionByAffiliation.bDetectNeutrals=true;
	mAISight->SetMaxAge(.1f);

	mAIPerception =  CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	mAIPerception->ConfigureSense(*mAISight);
	mAIPerception->SetDominantSense(mAISight->GetSenseImplementation());
	mAIPerception->OnPerceptionUpdated.AddDynamic(this, &AEnemyCharacter::OnSensedCallback);
	
}

void AEnemyCharacter::OnHitCallback(AActor* apDamagedActor, float aDamage, const UDamageType* apDamageType,
	AController* apInstigatedBy, AActor* apDamageCauser)
{
	ScreenPrintText("me han dañado: ");
	ScreenPrintWithFormat(Format1("%f",aDamage));

	APlayerCharacter* pDamager {Cast<APlayerCharacter>(apInstigatedBy->GetPawn())};
	if(pDamager)
	{
		pDamager->evAddMoney.Broadcast(this->mMoney);
	}
}

void AEnemyCharacter::OnSensedCallback(const TArray<AActor*>& Actors)
{
	
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

