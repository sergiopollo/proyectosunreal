// Fill out your copyright notice in the Description page of Project Settings.


#include "BPC_Weapon.h"
#include "PlayerCharacter.h"
#include "Algo/RandomShuffle.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Shooter23SMonfort/Public/Utils.h"

// Sets default values for this component's properties
UBPC_Weapon::UBPC_Weapon()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	mpMuzzleOffset = CreateDefaultSubobject<UBillboardComponent>(TEXT("ShootPoint"));
	
	
}

void UBPC_Weapon::PickupWeapon(APlayerCharacter* apPlayerCharacter)
{
	mpOwnerCharacter=apPlayerCharacter;
	if(mpOwnerCharacter!=nullptr && apPlayerCharacter->mMoney >= this->mPrice)
	{
		apPlayerCharacter->evAddMoney.Broadcast(-this->mPrice);
		
		if(mpOwnerCharacter->mpWeaponEquipped != nullptr)
			mpOwnerCharacter->mpWeaponEquipped->GetOwner()->Destroy();
		
		mpOwnerCharacter->mpWeaponEquipped=this;
		FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};

		GetOwner()->AttachToComponent(mpOwnerCharacter->GetWeapon(), AttachRules, FName(TEXT("WeaponPointSocket")));

		mpOwnerCharacter->evOnClick.AddDynamic(this, &UBPC_Weapon::OnFireCallback);
	}
}

void UBPC_Weapon::OnFireCallback()
{
	for (int i = 0; i < mBulletsPerShot; i++ )
	{
		Fire();
	}
}

void UBPC_Weapon::Fire()
{
	ScreenPrintText("pew");
	if(mpOwnerCharacter==nullptr) return;

	const UWorld* pWorld {GetWorld()};
	if(pWorld==nullptr) return;

	UCameraComponent* pPlayerCamera = {mpOwnerCharacter->GetCamera()};

	const FRotator CameraRotation {pPlayerCamera->GetComponentRotation()};

	//desde el frente de la camara
	//const FVector StartLocation {pPlayerCamera->GetComponentLocation()};
	
	//desde el cañon del arma
	const FVector StartLocation {this->GetOwner()->GetActorLocation() + CameraRotation.RotateVector(mpMuzzleOffset->GetComponentLocation())};

	const FVector EndLocation {StartLocation+((UKismetMathLibrary::GetForwardVector(pPlayerCamera->GetComponentRotation()) * mShootRange)+FVector(FMath::RandRange(-mDispersion, mDispersion), FMath::RandRange(-mDispersion, mDispersion), FMath::RandRange(-mDispersion, mDispersion)))};

	FCollisionQueryParams QueryParams {};
	QueryParams.AddIgnoredActor(mpOwnerCharacter);

	FHitResult HitResult {};
	pWorld->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera, QueryParams);
	DrawDebugLine(pWorld, StartLocation, EndLocation, HitResult.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 10.0f);

	if(HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
	{
		ScreenPrintWithFormat(Format1("%s",*HitResult.GetActor()->GetName()));
		UGameplayStatics::ApplyDamage(HitResult.GetActor(), mDamage, mpOwnerCharacter->GetController(), this->GetOwner(), {});
	}

}


// Called when the game starts
void UBPC_Weapon::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UBPC_Weapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

