// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Components/CapsuleComponent.h"
#include "Utils.h"
#include "Shooter23SMonfort/Public/Utils.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mWeaponPoint = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponPoint"));
	mWeaponPoint->bCastDynamicShadow=false;
	mWeaponPoint->CastShadow=false;

	mpCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	mpCamera->SetupAttachment(GetCapsuleComponent());
	mpCamera->bUsePawnControlRotation=true;

	evAddMoney.AddDynamic(this, &APlayerCharacter::AddMoney);
	
}

void APlayerCharacter::AddMoney(int newMoney)
{
	if(mMoney+newMoney < 0)
	{
		mMoney = 0;
	}
	else
	{
		mMoney = mMoney+newMoney;
	}
	ScreenPrintWithFormat(Format1("%i", mMoney))
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	mWeaponPoint->AttachToComponent(GetMesh(), {EAttachmentRule::SnapToTarget, true}, TEXT("WeaponPointSocket"));
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	APlayerController* pPlayerController =  Cast<APlayerController>(GetController());
	UEnhancedInputLocalPlayerSubsystem* pEInputSubsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(pPlayerController->GetLocalPlayer());
	pEInputSubsystem->ClearAllMappings();
	pEInputSubsystem->AddMappingContext(mInputMappingContext, 0);

	UEnhancedInputComponent* pEInputComponent {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	pEInputComponent->BindAction(mInputConfigData->InputMove, ETriggerEvent::Triggered, this, &APlayerCharacter::MoveCallback);
	pEInputComponent->BindAction(mInputConfigData->InputLook, ETriggerEvent::Triggered, this, &APlayerCharacter::LookCallback);
	pEInputComponent->BindAction(mInputConfigData->InputJump, ETriggerEvent::Triggered, this, &ACharacter::Jump);
	pEInputComponent->BindAction(mInputConfigData->InputMouseClick, ETriggerEvent::Started, this, &APlayerCharacter::OnClickCallback);
	
}


void APlayerCharacter::MoveCallback(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		const FVector2d MoveValue {aValue.Get<FVector2d>()};
		const FRotator MoveRotator {0,Controller->GetControlRotation().Yaw,0};

		if(MoveValue.Y != 0.0f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(Dir, MoveValue.Y);
		}
		if(MoveValue.X != 0.0f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(Dir, MoveValue.X);
		}
		
	}
}


void APlayerCharacter::LookCallback(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		const FVector2d LookValue {aValue.Get<FVector2d>()};

		if(LookValue.Y != 0)
		{
			AddControllerPitchInput(LookValue.Y);
		}
		if(LookValue.X != 0)
		{
			AddControllerYawInput(LookValue.X);
		}
		
	}
}

void APlayerCharacter::OnClickCallback(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		evOnClick.Broadcast();
		//UE_LOG(LogTemp, Warning, TEXT("shoot"));
		LogPrint("aaaawebo");
		if(GEngine != nullptr)
		{
			ScreenPrint(FString::Printf(TEXT("AAA %d"), mMoveSpeed));
		}
	}
}
