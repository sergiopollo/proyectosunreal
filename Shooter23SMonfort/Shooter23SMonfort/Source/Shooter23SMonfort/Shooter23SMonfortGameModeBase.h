// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Shooter23SMonfortGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER23SMONFORT_API AShooter23SMonfortGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
