ESTRATÈGIA - Sergio Monfort

Risketos Bàsics

 - [X] Control de personatge amb base ACharacter (o amb AController).
 - [X] Capacitat de disparar, ja sigui amb Trace line (Raycast) o amb objectes físics en moviment (Projectile).
 - [X] Ha d’implementar una petita inteligencia artificial als enemics.
 - [X] Utilitzar delegats per fer alguna acció, per exemple el disparar.
 - [X] Més d’un tipus d'armes i/o bales.
 - [X] Ús de les UMacros per privatització, escritura i lectura de forma correcta.

Risketos Opcionals

 - [X] Sistema de economia amb diners per dispar i compra d'armes

Controls

 - LEFT CLICK per disparar