RUNNER - Sergio Monfort

Risketos Bàsics

 - [X] Controls simples, ja sigui clicar un o dos botons.
 - [X] Moviment amb collisions.
 - [X] Aleatorietat.
 - [X] Creació d’objectes per Blueprint, ja sigui d’inici o un spawner (Relacionat amb el 1r Risketo Opcional).
 - [X] Puntuació mostrada per HUD, no val debug.

Risketos Opcionals

 - [ ] Fer una pool d’objectes.
 - [X] Augment progressiu de dificultat.
 - [X] Mort i reinici.
 - [X] Ús de parallax o altre format avançat de background.

Controls

 - SPACEBAR per saltar
 - A i D per moure's horitzontalment
